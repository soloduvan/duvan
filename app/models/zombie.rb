class Zombie < ApplicationRecord
	has_many :brains
	
	validates :bio, length: { maximum: 100 }
	validates :name, presence: true
	validates :age, numericality: {only_integer: true, message: "Solo numeros enteros pls <3"} 
  	validates :email, format: { :with => /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/ , message: "El formato del correo es invalido" }
	
	mount_uploader :avatar, AvatarUploader
end
