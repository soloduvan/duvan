class Brain < ApplicationRecord
	belongs_to :zombie
	
	validates :flavor, presence: true
	validates :iq, numericality: {only_integer: true, message: "Solo numeros enteros pls <3"}
end
