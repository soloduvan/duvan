class AddIqToBrains < ActiveRecord::Migration[5.1]
  def change
    add_column :brains, :iq, :integer
  end
end
