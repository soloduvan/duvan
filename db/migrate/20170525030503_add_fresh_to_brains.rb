class AddFreshToBrains < ActiveRecord::Migration[5.1]
  def change
    add_column :brains, :fresh, :boolean
  end
end
